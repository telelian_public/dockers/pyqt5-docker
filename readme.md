# pyqt5 docker

## build
```bash
docker build -t registry.gitlab.com/telelian_public/dockers/pyqt5-docker .
docker push registry.gitlab.com/telelian_public/dockers/pyqt5-docker
```

## pull
```bash
docker pull registry.gitlab.com/telelian_public/dockers/pyqt5-docker
```

## run
```bash
export DISPLAY=:0
xhost +local:docker

docker run -it --privileged --rm \
--net=host \
-v /tmp/.X11-unix:/tmp/.X11-unix \
-v /tmp/argus_socket:/tmp/argus_socket \
-e DISPLAY=unix:0 \
-v $HOME/work:/root/work \
registry.gitlab.com/telelian_public/dockers/pyqt5-docker /bin/bash
```