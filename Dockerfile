# build
FROM registry.gitlab.com/telelian/dock/jetson-nano-ubuntu-python-gstreamer:1.14.5-3.9.7-bionic-build as build

# Make sure we use the virtualenv:
RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

RUN apt-get update -qq \
    && apt-get install --no-install-recommends -y -qq \
    qt5-default pyqt5-dev pyqt5-dev-tools python3-pyqt5 

COPY requirements.txt ./
ENV MAKEFLAGS='-j$(nproc)'
RUN /opt/venv/bin/python -m pip install --upgrade pip wheel
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --no-cache-dir PyQt5==5.15.8 --config-settings --confirm-license= -vvv

# product
FROM registry.gitlab.com/telelian/dock/jetson-nano-ubuntu-python-l4t-gstreamer:1.14.5-32.7.1-3.9.7-bionic-run

# without l4t; need to volumes(/dev, /etc, /usr)
# FROM registry.gitlab.com/telelian/dock/jetson-nano-ubuntu-python-gstreamer:1.14.5-3.9.7-bionic-run

ENV LC_ALL C
ENV DEBIAN_FRONTEND noninteractive

RUN sed -i 's/# deb/deb/g' /etc/apt/sources.list

RUN apt-get update -qq \
    && apt-get install --no-install-recommends -y -qq \
    qt5-default pyqt5-dev pyqt5-dev-tools python3-pyqt5 \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh /
RUN chmod +x /wait-for-it.sh

ENV PYTHONUNBUFFERED=1

COPY --from=build /opt/venv /opt/venv
COPY . /opt/venv/
WORKDIR /opt/venv

# without l4t; need to volumes(/dev, /etc, /usr)
# RUN ln -sf /usr/bin/python3.9 /opt/venv/bin/python

ENV PATH=/opt/venv/bin:/opt/venv:$PATH

# CMD ["python", "-m", "vdc"]
